package com.plainjoestudio.pixelbright;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by frankayars on 6/20/17.
 */
public class ColorWheel extends View
{
    final private int bandWidth = 100;
    final private int bandRadius = 320;
    final private int svRadius = 180;
    private float svSquareSide = 0f;

    private boolean hueControl = true;

    private int selectedColor;
    private float selectedHue;

    private ColorPickerActivity pickerActivity = (ColorPickerActivity)getContext();

    private int xIntersect = 0;
    private int yIntersect = 0;

    private int xsvIntersect = 0;
    private int ysvIntersect = 0;

    private float xDebug = 0f;
    private float yDebug = 0f;

    private float cx = 0f;
    private float cy = 0f;

    private float [] hsvHueWheel = {0f, 1f, 1f};
    private float [] hsvSVWheel = {0f, 1f, 1f};

    public ColorWheel(final Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    Paint svPaint = null;
    LinearGradient linearGradient = null;

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);

        //svPaint.setStyle(Paint.Style.FILL);

        cx = ((float)getMeasuredWidth()  / 2f);
        cy = ((float)getMeasuredHeight() / 2f);

        float length = bandRadius - (bandWidth/2f);
        xIntersect = Math.round(cx);
        yIntersect = Math.round(cy - length);

        float diameter = svRadius*2;
        svSquareSide = (float) Math.sqrt((diameter*diameter)/2);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        super.onTouchEvent(event);

        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_DOWN:
                if (event.getPointerCount() == 1)
                {
                    int x = (int) event.getX();
                    int y = (int) event.getY();

                    float dx = (float)x - cx;
                    float dy = (float)y - cy;

                    float mag = (float)Math.sqrt((dx*dx) + (dy*dy));

                    hueControl = mag > svRadius;

                }
                break;
            case MotionEvent.ACTION_UP:
                if (event.getPointerCount() == 1)
                {

                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                if (event.getPointerCount() == 2)
                {
                    int x = (int) event.getX(1);
                    int y = (int) event.getY(1);
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:
                if (event.getPointerCount() == 2)
                {
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (event.getPointerCount() == 2)
                {

                    int x = (int) event.getX(1);
                    int y = (int) event.getY(1);

                }
                else if (event.getPointerCount() == 1)
                {
                    int x = (int) event.getX();
                    int y = (int) event.getY();

                    xDebug = x;
                    yDebug = y;

                    float dx = (float)x - cx;
                    float dy = (float)y - cy;

                    float mag = (float)Math.sqrt((dx*dx) + (dy*dy));

                    //normalize
                    float length = bandRadius - (bandWidth/2f);

                    float [] hsvColor = new float[3];

                    //are we controlling hue or h+s+v?
                    if(hueControl == false)
                    {
                        length = svRadius - 2;
                    }

                    dx /= mag;
                    dy /= mag;


                    if(hueControl == false)
                    {
                        if(mag > svRadius)
                        {
                            xsvIntersect = Math.round(cx + length * dx);
                            ysvIntersect = Math.round(cy + length * dy);
                        }
                        else
                        {
                            xsvIntersect = x;
                            ysvIntersect = y;
                        }
                    }
                    else
                    {
                        selectedHue = getAngle(dx, dy);
                        Log.d("hue", "hue(" + selectedHue + ")");
                        xIntersect = Math.round(cx + length * dx);
                        yIntersect = Math.round(cy + length * dy);
                    }

                    setDrawingCacheEnabled(true);
                    Bitmap b = getDrawingCache();
                    int color = b.getPixel(xsvIntersect, ysvIntersect);
                    Color.colorToHSV(color, hsvColor);

                    selectedColor = Color.HSVToColor(hsvColor);
                    pickerActivity.setNewColor(selectedColor);
                    invalidate();

                }
                break;
        }

        return true;
    }

    @Override
    public void draw(Canvas canvas)
    {
        super.draw(canvas);

        drawHue(canvas);
        drawHueSelector(canvas);
        drawSV(canvas);
        drawSVSelector(canvas);
        //drawDebug(canvas);

    }

    private void drawDebug(Canvas canvas)
    {
        Paint paint = new Paint();

        //DEBUG
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(6f);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawLine(cx, cy, xDebug, yDebug, paint);

        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(3f);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawLine(cx, cy, xIntersect, yIntersect, paint);
    }

    private void drawHueSelector(Canvas canvas)
    {
        //hue selector
        Paint paint = new Paint();

        float [] hsv = {selectedHue, 1f, 1f};
        int color = Color.HSVToColor(hsv);
        paint.setColor(color);
        paint.setStrokeWidth(3f);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(xIntersect, yIntersect, (bandWidth/2f), paint);

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(xIntersect, yIntersect, (bandWidth/2f), paint);
    }

    private void drawSVSelector(Canvas canvas)
    {
        //hue selector
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(xsvIntersect, ysvIntersect, 10, paint);
    }

    private void drawHue(Canvas canvas)
    {
        //HUE
        //start point
        float tx = cx;
        float ty = cy - bandRadius;

        float bx = tx;
        float by = ty + bandWidth;

        for(float hue=0f; hue<360f; hue+=0.5)
        {
            hsvHueWheel[0] = hue;
            int color = Color.HSVToColor(hsvHueWheel);

            //start at center of view
            //rotate around center
            //line endpoint is (view width - 20dp padding)
            //line startpoint is (endpoint - 40dp)

            float radians = (float)Math.toRadians(hue);

            float s = (float)Math.sin(radians);
            float c = (float)Math.cos(radians);

            // translate point back to origin:
            float txt = tx - cx;
            float tyt = ty - cy;

            float nx = txt * c - tyt * s;
            float ny = txt * s + tyt * c;

            // translate point back:
            float txn = nx + cx;
            float tyn = ny + cy;

            float bxt = bx - cx;
            float byt = by - cy;

            // rotate point
            nx = bxt * c - byt * s;
            ny = bxt * s + byt * c;

            // translate point back:
            float bxn = nx + cx;
            float byn = ny + cy;

            Paint paint = new Paint();
            paint.setColor(color);
            paint.setStrokeWidth(3f);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawLine(txn, tyn, bxn, byn, paint);
        }
        //HUE
    }

    private void drawSV(Canvas canvas)
    {
        if (svPaint == null)
        {
            svPaint = new Paint();
            //vertical gradient
            linearGradient = new LinearGradient(cx-svSquareSide/2, cy-svSquareSide/2, cx-svSquareSide/2, cy+svSquareSide/2, 0xffffffff, 0xff000000, Shader.TileMode.CLAMP);
        }


        hsvSVWheel[0] = selectedHue;
        hsvSVWheel[1] = 1f;
        hsvSVWheel[2] = 1f;
        int color = Color.HSVToColor(hsvSVWheel);
        //horizontal gradient
        Shader gradientShader = new LinearGradient(cx-svSquareSide/2, cy-svSquareSide/2, cx+svSquareSide/2, cy-svSquareSide/2, 0xffffffff, color, Shader.TileMode.CLAMP);
        ComposeShader shader = new ComposeShader(linearGradient, gradientShader, PorterDuff.Mode.MULTIPLY);
        svPaint.setShader(shader);

        //hack to get shader to work.
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        //canvas.drawRect(cx-svRadius, cy-svRadius, cx+svRadius, cy+svRadius, svPaint);
        canvas.drawCircle(cx, cy, svRadius, svPaint);
    }

    private float getAngle(float xDelta, float yDelta)
    {
        float angle;

        angle = (float)Math.atan2(yDelta, xDelta);
        angle = (float)Math.toDegrees(angle);
        angle += 90f;
        if(angle < 0f)
        {
            angle += 360f;
        }
        return angle;
    }

    static double normalizeToScale(double val, double A, double B, double C, double D)
    {
        double newVal;

        //Old Scale: A - B
        //New Scale: C - D

        newVal = C + (val-A)*(D-C)/(B-A);

        return newVal;
    }
}
