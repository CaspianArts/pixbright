package com.plainjoestudio.pixelbright;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
{
    private PixelGridFragment pixelGridFragment = null;

    private ImageButton menuButton;
    private ImageButton maskButton;
    private FrameLayout fragmentContainer;
    private ImageView gridLabel;

    private RelativeLayout menuView;
    private FrameLayout savedView;

    private LinearLayout savedButton;
    private LinearLayout grid1Button;
    private LinearLayout grid2Button;
    private LinearLayout grid3Button;
    private LinearLayout grid4Button;
    private LinearLayout grid5Button;
    private ImageView menuCloseButton;
    private ImageView savedCloseButton;

    private RecyclerView recyclerView;
    private SavedListAdapter savedItemsAdapter;
    private List<MenuItem> savedItemsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        removeNav();

        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        menuView = (RelativeLayout) findViewById(R.id.menu);
        savedView = (FrameLayout) findViewById(R.id.saved);
        gridLabel = (ImageView) findViewById(R.id.gridLabel);

        savedButton = (LinearLayout) findViewById(R.id.saveButton);
        grid1Button = (LinearLayout) findViewById(R.id.grid1Button);
        grid2Button = (LinearLayout) findViewById(R.id.grid2Button);
        grid3Button = (LinearLayout) findViewById(R.id.grid3Button);
        grid4Button = (LinearLayout) findViewById(R.id.grid4Button);
        grid5Button = (LinearLayout) findViewById(R.id.grid5Button);
        menuCloseButton = (ImageView) findViewById(R.id.menuCloseButton);
        savedCloseButton = (ImageView) findViewById(R.id.savedCloseButton);

        menuView.setVisibility(View.INVISIBLE);

        menuButton = (ImageButton)findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v)
            {
                //Intent intent = new Intent(MainActivity.this, MenuActivity2.class);
                //startActivityForResult(intent, 1);

                // Start the animation

                menuView.setX(-menuView.getWidth());
                menuView.setVisibility(View.VISIBLE);

                menuView.animate()
                        .setDuration(250)
                        .translationX(0)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                            }
                        });
            }
        });


        MenuClickListener menuClickListener = new MenuClickListener();
        SavedClickListener savedClickListener = new SavedClickListener();

        savedButton.setOnClickListener(menuClickListener);
        grid1Button.setOnClickListener(menuClickListener);
        grid2Button.setOnClickListener(menuClickListener);
        grid3Button.setOnClickListener(menuClickListener);
        grid4Button.setOnClickListener(menuClickListener);
        grid5Button.setOnClickListener(menuClickListener);
        menuCloseButton.setOnClickListener(menuClickListener);
        savedCloseButton.setOnClickListener(savedClickListener);

        maskButton = (ImageButton)findViewById(R.id.maskButton);
        maskButton.setVisibility(View.INVISIBLE);

        //maskButton.setImageResource(R.drawable.icon_mask_circle);
        maskButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v)
            {
                if(pixelGridFragment != null)
                {
                    int buttonImageId = R.drawable.icon_mask_circle;
                    if (pixelGridFragment.getMaskId() == 0)
                    {
                        pixelGridFragment.setMaskId(1);
                        buttonImageId = R.drawable.icon_mask_circle;
                    }
                    else if (pixelGridFragment.getMaskId() == 1)
                    {
                        pixelGridFragment.setMaskId(2);
                        buttonImageId = R.drawable.icon_mask_star;
                    }
                    else if (pixelGridFragment.getMaskId() == 2)
                    {
                        pixelGridFragment.setMaskId(0);
                        buttonImageId = R.drawable.icon_mask_square;
                    }
                    //maskButton.setImageResource(buttonImageId);
                    pixelGridFragment.saveGrid();
                }
            }
        });

        //saved items menu
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        savedItemsAdapter = new SavedListAdapter(this, savedItemsList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(savedItemsAdapter);
    }

    private int dpToPx(int dp)
    {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed()
    {
        if(savedView.getVisibility() == View.VISIBLE)
        {
            //simulate close button
            menuCloseButton.performClick();
        }
        else if(menuView.getVisibility() == View.VISIBLE)
        {
            //simulate close button
            savedCloseButton.performClick();
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        removeNav();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState)
    {

    }

    public void removeNav()
    {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public void setMaskButton(int maskId)
    {
        /*
        //default
        int buttonImageId = R.drawable.icon_mask_circle;
        if(maskId == 0)
        {
            buttonImageId = R.drawable.icon_mask_square;
        }
        else if(maskId == 1)
        {
            buttonImageId = R.drawable.icon_mask_circle;
        }
        else if(maskId == 2)
        {
            buttonImageId = R.drawable.icon_mask_star;
        }

        maskButton.setImageResource(buttonImageId);
        */
    }

    public void setGridLabel(int colCount)
    {
        gridLabel.setVisibility(View.VISIBLE);
        if(colCount == 2)
        {
            gridLabel.setImageResource(R.drawable.string_2x3);
        }
        else if(colCount == 4)
        {
            gridLabel.setImageResource(R.drawable.string_4x6);
        }
        else if(colCount == 8)
        {
            gridLabel.setImageResource(R.drawable.string_8x12);
        }
        else if(colCount == 16)
        {
            gridLabel.setImageResource(R.drawable.string_16x24);
        }
        else if(colCount == 32)
        {
            gridLabel.setImageResource(R.drawable.string_32x48);
        }
    }

    public void loadGrid(String name, int colCount, int rowCount)
    {

        //name is null if new grid

        if (fragmentContainer != null)
        {
            if(pixelGridFragment != null)
            {
                getSupportFragmentManager().beginTransaction()
                                           .remove(pixelGridFragment).commit();
            }

            maskButton.setVisibility(View.VISIBLE);

            // Create a new Fragment to be placed in the activity layout
            pixelGridFragment = new PixelGridFragment();
            pixelGridFragment.columnCount = colCount;
            pixelGridFragment.rowCount = rowCount;
            pixelGridFragment.name = name;
            pixelGridFragment.setMaskId(1); //circle by default

            setGridLabel(colCount);

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                                       .replace(R.id.fragment_container, pixelGridFragment).commitAllowingStateLoss();


        }

        //if menus are open, close them
        if(menuView.getVisibility() == View.VISIBLE)
        {
            //simulate close button
            menuCloseButton.performClick();
        }

        if(savedView.getVisibility() == View.VISIBLE)
        {
            //simulate close button
            savedCloseButton.performClick();
        }
    }

    private class MenuClickListener implements View.OnClickListener
    {
        // ...

        @Override
        public void onClick(View v)
        {
            boolean closeMenu = true;

            if(v == savedButton)
            {
                //be sure to save the current grid
                if(pixelGridFragment != null)
                {
                    pixelGridFragment.saveGrid();
                }

                //now load the saved menu
                savedItemsList.clear();

                File[] files = getFilesDir().listFiles();
                Log.d("Files", "Size: "+ files.length);
                int boardNumber = 1;
                for (int i = 0; i < files.length; i++)
                {
                    if(files[i].getName().startsWith("pixelgrid-") == true)
                    {
                        String fileName = files[i].getName();
                        String thumbFileName = fileName.replaceFirst("pixelgrid-", "pixelgridthumb-") + ".jpg";

                        MenuItem menuItem = new MenuItem();
                        menuItem.name = "BOARD #" + boardNumber;
                        boardNumber++;
                        menuItem.fileName = fileName;
                        menuItem.thumbFileName = thumbFileName;
                        menuItem.type = "open";
                        savedItemsList.add(menuItem);

                        Log.d("Files", "FileName:" + files[i].getName());
                    }
                }
                savedItemsAdapter.notifyDataSetChanged();

                savedView.setX(-savedView.getWidth());
                savedView.setVisibility(View.VISIBLE);

                savedView.animate()
                         .setDuration(250)
                         .translationX(0)
                         .setListener(new AnimatorListenerAdapter() {
                             @Override
                             public void onAnimationEnd(Animator animation) {
                                 super.onAnimationEnd(animation);

                             }
                         });

                closeMenu = false;
            }
            else if(v == grid1Button)
            {
                loadGrid("", 2, 3);
            }
            else if(v == grid2Button)
            {
                loadGrid("", 4, 6);
            }
            else if(v == grid3Button)
            {
                loadGrid("", 8, 12);
            }
            else if(v == grid4Button)
            {
                loadGrid("", 16, 24);
            }
            else if(v == grid5Button)
            {
                loadGrid("", 32, 48);
            }
            else if(v == menuCloseButton)
            {
                //do nothing here, but animate out...
            }

            if(closeMenu == true)
            {
                menuView.animate()
                        .setDuration(250)
                        .translationX(-menuView.getWidth())
                        .setListener(new AnimatorListenerAdapter()
                        {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                                super.onAnimationEnd(animation);
                                menuView.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        }
    }

    private class SavedClickListener implements View.OnClickListener
    {
        // ...

        @Override
        public void onClick(View v)
        {

            savedView.animate()
                    .setDuration(250)
                    .translationX(-savedView.getWidth())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            savedView.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }
}
