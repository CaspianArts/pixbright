package com.plainjoestudio.pixelbright;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Date;


/**
 * Created by frankayars on 6/2/17.
 */
public class PixelView extends View
{
    private final float startWhite = 330f;
    private final float endWhite = 360.0f;
    private final float rangeWhite = (endWhite-startWhite);
    private final float fullWhite = endWhite - (rangeWhite/2f);

    private float [] hsvBase = new float[3];
    private float [] hsv = new float[3];

    private boolean isWaitingHueChange = false;

    private int hxStart;
    private int hyStart;

    private int vxStart;
    private int vyStart;

    //default to white...
    private float hue = fullWhite;
    private float saturation = 0.0f;
    private float value = 1.0f;

    private float hStart = fullWhite;
    private float hTemp = fullWhite;

    private float vStart = 1.0f;
    private float vTemp = 1.0f;

    private float vDirection = 1.0f;

    private Drawable mask;

    private boolean touched = false;
    //default
    private int maskId = 0; //circle mask default

    private long timestamp;

    public PixelView(final Context context, AttributeSet attrs)
    {
        super(context, attrs);

        //lock in default (for reset)
        hsvBase[0] = hue;
        hsvBase[1] = saturation;
        hsvBase[2] = value;

        setHSV(hue, saturation, value);
        int color = Color.HSVToColor(hsv);
        setBackgroundColor(color);
    }

    @Override
    public void draw(Canvas canvas)
    {
        super.draw(canvas);

        if(mask != null)
        {
            Rect imageBounds = canvas.getClipBounds();  // Adjust this for where you want it

            mask.setBounds(imageBounds);
            mask.draw(canvas);
        }
    }

    public void beginHueChange(int x, int y)
    {
        hxStart = x;
        hyStart = y;
        hStart = hue;
    }

    public void endHueChange()
    {
        hue = hTemp;
    }

    public void executeHueChange(int x, int y)
    {

        int xDelta = x - hxStart;
        int yDelta = hyStart - y;

        float xWeight = 1.0f;
        float yWeight = 1.0f;
        float angle = getSwipeAngle(xDelta, yDelta);

        if(angle >= 0f && angle <= 90f)
        {
            yWeight = (float)normalizeToScale(angle, 0, 90, 0, 1);
            xWeight = 1.0f - yWeight;
        }
        else if(angle > 90f && angle <= 180f)
        {
            xWeight = (float)normalizeToScale(angle, 90, 180, 0, 1);
            yWeight = 1.0f - xWeight;
        }
        else if(angle > 180f && angle <= 270f)
        {
            yWeight = (float)normalizeToScale(angle, 180, 270, 0, 1);
            xWeight = 1.0f - yWeight;
        }
        else if(angle > 270f && angle < 360f)
        {
            xWeight = (float)normalizeToScale(angle, 270, 360, 0, 1);
            yWeight = 1.0f - xWeight;
        }

        float magnitude = (xDelta*xWeight) + (yDelta*yWeight);

        //float magnitude = (float) Math.sqrt(Math.pow(xDelta, 2) + Math.pow(yDelta, 2));
        magnitude = (float) normalizeToScale(magnitude, 0, 1000, 0, 360);

        hTemp = hStart + magnitude;
        hTemp %= 360.0f;

        if(hTemp < 0)
        {
            hTemp = 360f;
            hStart = 360f;
        }

        //may be coming from white
        saturation = 1.0f;
        hsv[1] = saturation;

        hsv[0] = hTemp;

        int color = Color.HSVToColor(hsv);
        setBackgroundColor(color);
    }

    public void beginValueChange(int x, int y)
    {
        vxStart = x;
        vyStart = y;
        vStart = value;
    }

    public void endValueChange()
    {
        value = vTemp;
    }

    public void executeValueChange(int x, int y)
    {

        int xDelta = x - vxStart;
        int yDelta = vyStart - y;

        Log.d("hsv", "xD " + xDelta);

        float xWeight = 1.0f;
        float yWeight = 1.0f;
        float angle = getSwipeAngle(xDelta, yDelta);

        if(angle >= 0f && angle <= 90f)
        {
            yWeight = (float)normalizeToScale(angle, 0, 90, 0, 1);
            xWeight = 1.0f - yWeight;
        }
        else if(angle > 90f && angle <= 180f)
        {
            xWeight = (float)normalizeToScale(angle, 90, 180, 0, 1);
            yWeight = 1.0f - xWeight;
        }
        else if(angle > 180f && angle <= 270f)
        {
            yWeight = (float)normalizeToScale(angle, 180, 270, 0, 1);
            xWeight = 1.0f - yWeight;
        }
        else if(angle > 270f && angle < 360f)
        {
            xWeight = (float)normalizeToScale(angle, 270, 360, 0, 1);
            yWeight = 1.0f - xWeight;
        }

        float magnitude = (xDelta*xWeight) + (yDelta*yWeight);

        magnitude = (float) normalizeToScale(magnitude, 0, 500, 0, 1);

        vTemp = vStart + magnitude;
        vTemp = Math.min(vTemp, 1.0f);
        vTemp = Math.max(vTemp, 0.1f);

        Log.i("hsv", "vTemp: " + vTemp + " mag: " + magnitude);
        hsv[2] = vTemp;

        int color = Color.HSVToColor(hsv);
        setBackgroundColor(color);
    }

    public void setHSV(float hue, float saturation, float value)
    {
        hsv[0] = hue;
        hsv[1] = saturation;
        hsv[2] = value;

        this.value = value;
        vStart = value;
        vTemp = value;

        int color = Color.HSVToColor(hsv);
        setBackgroundColor(color);
    }

    public float[] getHSV()
    {
        return hsv;
    }

    public void resetHSV()
    {
        hsv[0] = hsvBase[0];
        hsv[1] = hsvBase[1];
        hsv[2] = hsvBase[2];

        hue = hsvBase[0];
        hStart = hsvBase[0];
        hTemp = hsvBase[0];

        value = hsvBase[2];
        vStart = hsvBase[2];
        vTemp = hsvBase[2];

        int color = Color.HSVToColor(hsv);
        setBackgroundColor(color);
    }

    public boolean isDefaultHSV()
    {
        boolean isDefault = false;

        if(hsv[0] == hsvBase[0] && hsv[1] == hsvBase[1] && hsv[2] == hsvBase[2])
        {
            isDefault = true;
        }

        return isDefault;
    }

    public void unSetHSV()
    {
        touched = false;
        setVisibility(View.INVISIBLE);
    }

    public void setTouched(boolean touched)
    {
        this.touched = touched;
        timestamp = new Date().getTime();
    }

    public boolean getTouched()
    {
        return touched;
    }

    public void setMaskId(int maskId)
    {
        this.maskId = maskId;

        //0 = square (no mask)
        if(maskId == 0)
        {
            mask = null;
        }
        else
        {
            int maskResourceId = R.drawable.circle_mask;
            if(maskId == 1)
            {
                maskResourceId = R.drawable.circle_mask;
            }
            else if(maskId == 2)
            {
                maskResourceId = R.drawable.circle_mask_small;
            }

            // R.drawable.circle_mask
            mask = ResourcesCompat.getDrawable(getResources(), maskResourceId, null);
        }
        this.invalidate();
    }

    public int getMaskId()
    {
        return maskId;
    }

    public long getTimestamp() { return timestamp; }

    public boolean getWaitingHueChange()
    {
        return isWaitingHueChange;
    }

    public void setWaitingHueChange(boolean waitingHueChange)
    {
        this.isWaitingHueChange = waitingHueChange;
    }

    private float getSwipeAngle(float xDelta, float yDelta)
    {
        float angle;

        angle = (float)Math.atan2(yDelta, xDelta);
        angle = (float)Math.toDegrees(angle);
        if(angle < 0)
        {
            angle += 360.0;
        }
        return angle;
    }

    static double normalizeToScale(double val, double A, double B, double C, double D)
    {
        double newVal;

        //Old Scale: A - B
        //New Scale: C - D

        newVal = C + (val-A)*(D-C)/(B-A);

        return newVal;
    }



}
