package com.plainjoestudio.pixelbright;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

public class PixelGridFragment extends Fragment
{
    private UUID id = null;

    public int columnCount = 2;
    public int rowCount = 2;
    private int maskId;

    public String name = "";

    private PixelView currentPixelView = null;

    //pass the touch to the gesture detector
    private GestureDetector gestureDetector;

    private ArrayList<PixelView> pixels = new ArrayList<>();

    public boolean pauseTouches = false;

    public PixelGridFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState)
    {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_pixel_grid, container, false);

        final FragmentActivity rootActivity = (FragmentActivity) rootView.getContext();

        LinearLayout pixelGridParent = (LinearLayout) rootView.findViewById(R.id.pixelGridParent);

        PixelGrid pixelGrid = null;
        if(name.length() > 0)
        {
            pixelGrid = openGrid(name);
            this.columnCount = pixelGrid.columnCount;
            this.rowCount = pixelGrid.rowCount;
            this.id = pixelGrid.id;
            this.maskId = pixelGrid.maskId;

            MainActivity mainActivity = (MainActivity) rootActivity;
            mainActivity.setMaskButton(maskId);
            mainActivity.setGridLabel(this.columnCount);
        }

        //dp formula
        //pixels = dps * (density / 160)

        DisplayMetrics metrics = new DisplayMetrics();
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        //int height = size.y;

        //square pixels (fit to width)
        int pixelWidth = width/this.columnCount;
        int pixelHeight = pixelWidth;

        int count = 0;
        //float cellWeight = 1.0f / (float) this.columnCount;
        for (int y = 0; y < this.rowCount; y++)
        {
            LinearLayout row = new LinearLayout(rootActivity);

            LinearLayout.LayoutParams rowLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixelHeight);
            row.setLayoutParams(rowLayoutParams);
            row.setOrientation(LinearLayout.HORIZONTAL);
            pixelGridParent.addView(row);

            for (int x = 0; x < this.columnCount; x++)
            {
                LinearLayout.LayoutParams cellLayoutParams = new LinearLayout.LayoutParams(pixelWidth, pixelHeight);
                PixelView pixelView = new PixelView(this.getContext(), null);
                //not visible until first touch

                boolean touched = false;
                if(pixelGrid != null)
                {
                    touched = pixelGrid.touched[count];
                }
                pixelView.setTouched(touched);
                if(touched == false)
                {
                    pixelView.setVisibility(View.INVISIBLE);
                }

                pixelView.setMaskId(maskId);

                row.addView(pixelView, cellLayoutParams);
                pixels.add(pixelView);

                if(pixelGrid != null)
                {
                    float [] hsv = pixelGrid.pixels[count];
                    pixelView.setHSV(hsv[0], hsv[1], hsv[2]);
                }
                count++;
            }

        }

        gestureDetector = new GestureDetector(getContext(), new GestureListener(rootView, container));

        rootView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                gestureDetector.onTouchEvent(event);

                if(pauseTouches == false)
                {
                    switch (event.getAction() & MotionEvent.ACTION_MASK)
                    {
                        case MotionEvent.ACTION_DOWN:
                            if (event.getPointerCount() == 1)
                            {
                                int x = (int) event.getRawX();
                                int y = (int) event.getRawY();
                                currentPixelView = findPixel(x, y);
                                if (currentPixelView != null)
                                {
                                    if(currentPixelView.getTouched() == false)
                                    {
                                        currentPixelView.setTouched(true);
                                        currentPixelView.setVisibility(View.VISIBLE);
                                    }

                                    pauseTouches = true;

                                    currentPixelView.setWaitingHueChange(true);

                                    new android.os.Handler().postDelayed(
                                            new Runnable()
                                            {
                                                public void run()
                                                {
                                                    pauseTouches = false;
                                                }
                                            },
                                            150);
                                }

                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            if (event.getPointerCount() == 1)
                            {
                                if (currentPixelView != null)
                                {
                                    currentPixelView.endHueChange();
                                    currentPixelView = null;
                                }
                            }
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            if (event.getPointerCount() == 2)
                            {
                                if (currentPixelView != null)
                                {
                                    int x = (int) event.getX(1);
                                    int y = (int) event.getY(1);
                                    currentPixelView.beginValueChange(x, y);
                                }
                            }
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            if (event.getPointerCount() == 2)
                            {
                                if (currentPixelView != null)
                                {
                                    pauseTouches = true;
                                    currentPixelView.endValueChange();

                                    //delay other touches for 150ms avoiding incidental hue changes after
                                    //lifting finger (1)
                                    new android.os.Handler().postDelayed(
                                            new Runnable() {
                                                public void run() {
                                                    pauseTouches = false;
                                                }
                                            },
                                            150);
                                }
                            }
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (event.getPointerCount() == 2)
                            {
                                if (currentPixelView != null)
                                {
                                    int x = (int) event.getX(1);
                                    int y = (int) event.getY(1);
                                    currentPixelView.executeValueChange(x, y);
                                }
                            }
                            else if (event.getPointerCount() == 1)
                            {
                                if (currentPixelView != null)
                                {
                                    int x = (int) event.getX();
                                    int y = (int) event.getY();

                                    if(currentPixelView.getWaitingHueChange() == true)
                                    {
                                        currentPixelView.setWaitingHueChange(false);
                                        currentPixelView.beginHueChange(x, y);
                                    }
                                    else
                                    {
                                        currentPixelView.executeHueChange(x, y);
                                    }
                                }
                            }
                            break;
                    }
                }
                return true;
            }
        });

        return rootView;
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        Log.e("Rebright", "Low Memory Warning");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        saveGrid();
        Log.d("rebright", "OnPause, saveGrid()");
    }

    @Override
    public void onStop()
    {
        super.onStop();
        saveGrid();
        Log.d("rebright", "OnStop, saveGrid()");
    }

    private PixelView findPixel(int x, int y)
    {
        PixelView hitView = null;
        Iterator<PixelView> itr = pixels.iterator();
        while(itr.hasNext())
        {
            PixelView pixelView = itr.next();
            int[] location = new int[2];
            pixelView.getLocationOnScreen(location);
            Rect rect = new Rect(location[0], location[1], location[0] + pixelView.getWidth(), location[1] + pixelView.getHeight());
            if (rect.contains(x, y))
            {
                hitView = pixelView;
            }
        }

        return hitView;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private View view;
        private ViewGroup container;

        GestureListener(View view, ViewGroup container)
        {
            this.container = container;
            this.view = view;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e)
        {
            long currentTime = new Date().getTime();

            int x = (int)e.getRawX();
            int y = (int)e.getRawY();

            PixelView pixelView = findPixel(x, y);
            if(pixelView != null && pixelView.getTouched() == true)
            {
                long pixelViewLifetime = currentTime - pixelView.getTimestamp();
                //don't reset a newly created pixel (say less than 2 seconds)
                if(pixelViewLifetime > 2000)
                {
                    //delay any touch responses for 150ms (so we don't change the hue incidentally)
                    pauseTouches = true;
                    if (pixelView.isDefaultHSV() == true)
                    {
                        pixelView.unSetHSV();
                    }
                    else
                    {
                        pixelView.resetHSV();
                    }
                    new android.os.Handler().postDelayed(
                            new Runnable()
                            {
                                public void run()
                                {
                                    pauseTouches = false;
                                }
                            },
                            150);
                }
            }

            return true;
        }
    }

    public void setMaskId(int maskId)
    {
        this.maskId = maskId;

        Iterator<PixelView> itr = pixels.iterator();
        while(itr.hasNext() == true)
        {

            PixelView pixelView = itr.next();
            pixelView.setMaskId(maskId);
        }
    }

    public int getMaskId()
    {
        return maskId;
    }

    public PixelGrid openGrid(String fileName)
    {
        PixelGrid pixelGrid = null;

        FileInputStream inputStream;

        try
        {
            inputStream = getContext().openFileInput(fileName);

            StringBuffer json = new StringBuffer("");

            byte[] buffer = new byte[1024];

            int n;
            while ((n = inputStream.read(buffer)) != -1)
            {
                json.append(new String(buffer, 0, n));
            }

            Gson gson = new Gson();
            pixelGrid = gson.fromJson(json.toString(), PixelGrid.class);
            //Log.i("hsv", "" + pixelGrid.columnCount);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pixelGrid;
    }

    public void saveGrid()
    {
        if (hasBeenTouched() == true)
        {
            if (id == null)
            {
                id = UUID.randomUUID();
            }

            try
            {
                FileOutputStream outputStream;
                String fileName = "pixelgrid-" + id.toString();

                PixelGrid pixelGrid = new PixelGrid();
                pixelGrid.id = id;
                pixelGrid.columnCount = columnCount;
                pixelGrid.rowCount = rowCount;
                pixelGrid.maskId = maskId;

                outputStream = getContext().openFileOutput(fileName, Context.MODE_PRIVATE);

                int currentPixel = 0;
                Iterator<PixelView> itr = pixels.iterator();
                pixelGrid.pixels = new float[pixels.size()][3];
                pixelGrid.touched = new boolean[pixels.size()];

                while (itr.hasNext() == true)
                {

                    PixelView pixelView = itr.next();
                    float[] hsv = pixelView.getHSV();

                    pixelGrid.pixels[currentPixel] = hsv;
                    pixelGrid.touched[currentPixel] = pixelView.getTouched();

                    currentPixel++;
                }

                Gson gson = new Gson();
                String json = gson.toJson(pixelGrid);
                outputStream.write(json.getBytes());
                outputStream.close();

                getView().setDrawingCacheEnabled(true);
                Bitmap b = getView().getDrawingCache();

                fileName = "pixelgridthumb-" + id.toString() + ".jpg";
                b.compress(Bitmap.CompressFormat.JPEG, 95, getContext().openFileOutput(fileName, Context.MODE_PRIVATE));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private boolean hasBeenTouched()
    {
        boolean touched = false;
        Iterator<PixelView> itr = pixels.iterator();

        while(itr.hasNext() == true)
        {

            PixelView pixelView = itr.next();
            touched = pixelView.getTouched();
            if(touched == true)
            {
                break;
            }
        }

        return  touched;
    }
}