package com.plainjoestudio.pixelbright;

import java.util.UUID;

/**
 * Created by frankayars on 6/8/17.
 */
public class PixelGrid
{
    public UUID id = null;
    public int columnCount = 2;
    public int rowCount = 2;
    public float [][] pixels;
    public boolean [] touched;
    public int maskId;
}
