package com.plainjoestudio.pixelbright;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class ColorPickerActivity extends AppCompatActivity
{

    private ColorWheel colorWheel;
    private View newColor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_wheel);

        colorWheel = (ColorWheel)findViewById(R.id.color_wheel);
        newColor = findViewById(R.id.new_color);

    }

    public void setNewColor(int color)
    {
        newColor.setBackgroundColor(color);
    }
}
