package com.plainjoestudio.pixelbright;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by frankayars on 6/6/17.
 */
public class SavedListAdapter extends RecyclerView.Adapter<SavedListAdapter.MyViewHolder>
{
    private Context mContext;
    private List<MenuItem> savedItemList;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView title;
        public ImageView thumbnail;
        public String fileName;

        public MyViewHolder(View view)
        {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }


    public SavedListAdapter(Context mContext, List<MenuItem> savedItemList) {
        this.mContext = mContext;
        this.savedItemList = savedItemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.saved_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        MenuItem menuItem = savedItemList.get(position);
        holder.title.setText(menuItem.name);
        holder.fileName = menuItem.fileName;

        if(menuItem.thumbFileName != null && menuItem.thumbFileName.length() > 0)
        {
            try
            {
                FileInputStream inputStream = mContext.openFileInput(menuItem.thumbFileName);
                if (inputStream != null)
                {
                    Bitmap myBitmap = BitmapFactory.decodeStream(inputStream);
                    holder.thumbnail.setImageBitmap(myBitmap);

                    holder.thumbnail.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            MainActivity mainActivity = (MainActivity)mContext;
                            mainActivity.loadGrid(holder.fileName, 0, 0);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return savedItemList.size();
    }
}
